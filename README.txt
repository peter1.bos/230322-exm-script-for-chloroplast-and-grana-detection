Scripts created for the analysis of expanded chloroplast or unexpanded chloroplasts.
Scripts have been used in the article: Expansion microscopy on isolated chloroplasts (link Doi)

Five scripts for python (.py) or FIJI (.ijm) for the detection and clustering of chloroplasts and grana.

Specific requirement for FIJI that is needed to let python scripts run well. 
For FIJI, go to set measurements and tick the boxes:
Area
Standard deviation
Min & max gray values
Center of mass
Shape descriptors
Mean gray value
Modal grey value
Fit ellipse
Display label


Chloroplasts workflow:
Put Z-stacks in a folder
run macro "chloroplast detection.ijm" in FIJI
--Uses a threshold filter and some image tools to detect grana. Criteria for circularity and min and max area can be selected. Detects particles in all slices of the stack
run script "Cluster chloroplasts.py"
--Clusters chloroplasts based on X and Y position and returns its dimensions and intensity. 


Grana workflow
Put Z-stacks in a folder
run macro "Changepizelsize.ijm" and set the pixelsize equal for pre-expansion dimensions.
Follow the steps for detecting grana on https://github.com/HenriquesLab/ZeroCostDL4Mic/blob/master/Colab_notebooks/StarDist_2D_ZeroCostDL4Mic.ipynb
Use the model "221219_voxadjtopview-20230223T154247Z-001.zip"
In google colab in section "6.1 generate predistions from unseen dataset", at "What outputs would you like to generate?": select: "Region_of_interests: "
Download the ROIs and unzip the folder. 
Run macro "MeasureROIsfromdrive.ijm"
--Measures the ROIs in the original image to get unadapted dimensions.
Run resulting data through script: "Grana clustering.py"
--Clusters grana based on X and Y position and the slice number. Returns dimensions and intensity of the grana. 


Denoising: 
Follow instructions on https://colab.research.google.com/github/HenriquesLab/ZeroCostDL4Mic/blob/master/Colab_notebooks/Noise2Void_2D_ZeroCostDL4Mic.ipynb.
Use own images to train your own model. Use model export.bioimage.io.zip to denoise images used in this study. 