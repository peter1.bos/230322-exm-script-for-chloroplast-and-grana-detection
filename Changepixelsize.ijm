// get image IDs of all open images
input_path = getDirectory("input files");
filelist = getFileList(input_path);
dir = getDirectory("Where do you want to save images?");
//directory = getDirectory("Individual images");
Selectchannel = 1;
togovoxel = 0.06
togovoxel_Z = 0.06

for (Rijn = 0; Rijn < filelist.length; Rijn++) {
	open(input_path + filelist[Rijn]);
	title = getTitle();
	getDimensions( width, height, channels, slices, frames );
	helpwithchannel = channels + 1;
	if (channels >1) {
		run("Split Channels");
			
		for (waal = 1; waal < helpwithchannel ; waal++) {
			if (waal != Selectchannel) {
				selectWindow("C" + waal + "-" + title);
				close();
				}
			}
	}
	getDimensions( width, height, channels, slices, frames );
	fullwidth = width;
	fullheight = height;
	getVoxelSize(width, height, depth, unit);
	voxwidth = width;
	voxheight = height;
	voxdepth = depth;
	pixxy = (voxwidth/togovoxel) * fullheight;
	pixz = (voxdepth/togovoxel_Z)*slices;
	
	//Choose if you want to interpolate in Z (second line) or not (first line)	
	run("Size...", "width=" + pixxy + " height=" + pixxy + " constrain average interpolation=Bilinear");
	//run("Size...", "width=" + pixxy + " height=" + pixxy + " depth=" + pixz + " constrain average interpolation=Bilinear");
	saveAs("tiff", dir + title);
	//run("Image Sequence... ", "select=["+ directory + "] dir=[" + directory + "] format=TIFF");	
	run("Close All");
}
showMessage("Finito", "I hope I did my job well");