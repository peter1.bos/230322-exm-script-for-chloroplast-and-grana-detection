//Uses input from the stardist analysis. Images used in the stardist analysis are put in one folder 
//and ROIs detected by stardist in another folder. Choose where you save the results. 

Input_image = getDirectory("Input for exp Alexa-Chl images");
filelistimage = getFileList(Input_image);
Input_Rois = getDirectory("Input for ROIS please");
filelistroihelp = getFileList(Input_Rois);
dir = getDirectory("Where would you like the results saved?");

for (linge = 0; linge<filelistimage.length ; linge++) {
	roiManager("reset");
	open(Input_image + filelistimage[linge]);
	Merwede = getTitle();
	print(Merwede);
	Hollandsdiep = substring(Merwede, 0, lengthOf(Merwede) - 4);
	open(Input_Rois + Hollandsdiep + ".zip");
	//roimanager("open", Input_Rois + Hollandsdiep + ".zip")
	roiManager("deselect");
	roiManager("measure");
	filelistrois = getFileList(Input_Rois + Hollandsdiep);
	close("*");
}
saveAs("results", dir + "230213_fiji_2208_selectedgrana.csv");	
run("Clear Results");
roiManager("reset");
showMessage("Finito", "I hope I did my job well");
