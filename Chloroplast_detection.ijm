

input_path4 = getDirectory("Input for exp Alexa-Chl images");
//Selects the limits for the praticles that will be detected. 
exporunexp	= getBoolean("Are the following chloroplasts expanded or unexpanded?" , "Expanded" , "Unexpanded");
if (exporunexp == 1) {
	acceptedMean = 2;
	Minarea = 100; 
	Maxarea = 10000; 
	Mincirc = 0.30;
	Maxcirc = 1.00;
	histogr = 8;
}
if (exporunexp == 0 ) {
	acceptedMean = 3;
	Minarea = 0.5; 
	Maxarea = 75; 
	Mincirc = 0.70;
	Maxcirc = 1.00;
	histogr = 4;
}
dir = getDirectory("Where do you want to save the results table?");
filelist4 = getFileList(input_path4);
//In case of a multichannel image, select what channel you want analysed
Selectchannel = 1;


for (linge = 0; linge<filelist4.length ; linge++) {
	//open(input_path4 + filelist4[linge]);
	run("Bio-Formats Importer", "open=[" + input_path4 + filelist4[linge] + "] autoscale color_mode=Default rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT");
	getVoxelSize(width, height, depth, unit);
	getDimensions( width, height, channels, slices, frames );
	//Select the channel that you chose in the input part. First split and then close all that are not that channel. 
	helpwithchannel = channels + 1;
	Geul = 1;
	title = getTitle();
	if (channels >1) {
		Geul= 2;
		run("Split Channels");
			
		for (waal = 1; waal < helpwithchannel ; waal++) {
			if (waal != Selectchannel) {
				selectWindow("C" + waal + "-" + title);
				close();
				}
			}
	}
	roiManager("reset");
	
	//Select imahe   
	if (Geul == 2) {
		selectWindow("C" + Selectchannel + "-" + title);
		}
	//selects all channels one by one. Determines chlorosize in all (if particle is found) and puts it in a new row. Mean was default choice for expanded chloroplasts.	
	roiManager("reset");
	Original = getTitle();
	rename(Original + " Mean");
	Merwede_Mean = getTitle();
	run("Duplicate...", "duplicate");
	Merwe_copy = getTitle();
	setAutoThreshold("Mean dark");
	setOption("BlackBackground", true);
	run("Convert to Mask", "method=Mean background=Dark calculate black");
	run("Fill Holes", "stack");
	run("Erode", "stack");
	run("Erode", "stack");
	run("Dilate", "stack");
	run("Dilate", "stack");		
	run("Analyze Particles...",  "size=" + Minarea + "-" + Maxarea + " circularity=" + Mincirc + "-" + Maxcirc + " exclude add stack");
	close(Merwe_copy);
	selectWindow(Merwede_Mean);
	if (roiManager("Count") > 0){
		roiManager("Measure");
		roiManager("delete");	
		}
		
	//Do it again but then with default. Default was default choice for not expanded chloroplasts. 
	selectWindow(Merwede_Mean);
	rename(Original + " Default");
	Merwede_Default = getTitle();
	run("Duplicate...", "duplicate");
	Merwede_duplicate = getTitle();
	setAutoThreshold("Percentile dark");
	setOption("BlackBackground", true);
	run("Convert to Mask", "method=Default background=Dark calculate black");
	run("Fill Holes", "stack");
	run("Erode", "stack");
	run("Dilate", "stack");		
	run("Analyze Particles...",  "size=" + Minarea + "-" + Maxarea + " circularity=" + Mincirc + "-" + Maxcirc + " exclude add stack");
	close(Merwede_duplicate);
	selectWindow(Merwede_Default);
	if (roiManager("Count") > 0){
		roiManager("Measure");
		roiManager("delete");	
		}
	
		
	close(Merwede_Default);
	close("*");
	}

saveAs("results", dir + "INSERT_TITLE.csv");	
run("Clear Results");
showMessage("Finito", "I hope I did my job well");