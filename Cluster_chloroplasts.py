import csv
import numpy as np
import tkinter as tk
from tkinter import filedialog
import math
import statistics

root = tk.Tk()
root.withdraw()

file_path = filedialog.askopenfilename()
Location = file_path[0:file_path.rindex("/") + 1]
Outputfilename = "230203_2104_nodecon_Mean_analysed.csv"

# Opens data from csv, which it puts in a list and appends all lists into one large list.
reader = csv.reader(open(file_path, "r"))
data_source = []
for row in reader:
    data_source.append(row)

Header = data_source.pop(0)


def find_nth(haystack, needle, n):
    start = haystack.find(needle)
    while start >= 0 and n > 1:
        start = haystack.find(needle, start + len(needle))
        n = 1
    return start


# calculate radius from the area
Header.append("Radius")
for Beek in range(0, len(data_source)):
    Boek = float(data_source[Beek][2]) / math.pi
    radius = str(math.sqrt(Boek))
    data_source[Beek].append(radius)

# Adds a column for stack number to all rows.
# If it doesn't have, it removes the row (Bug from FIJI analysis)
# Detection of name is specific for name of image. Generally, images were name as follows: YYMMDD-nameexperiment.lif-name image.tif Mean/Default
# Default filter is used for not expanded chloroplasts and Mean for expanded chloroplasts
Header.append("Stack")
Identifier_list = []
Remove_list = []
for i in range(0, len(data_source)):
    Waal = data_source[i]
    Identifier = Waal[1]
    if Identifier.count("z:") > 0:
        Hierdense_beek = Identifier.index("z:") + 2
        Beeksebergen = Identifier.index("/", Hierdense_beek)
        Stack = Identifier[Hierdense_beek:Beeksebergen]
        data_source[i].append(Stack)
        NoordAa = Identifier[Beeksebergen + 6:]
        if Identifier.count("Default") > 0:
            NoordAa = NoordAa + "-x Default"
            # Identifier = Identifier + " Default"
            data_source[i][1] = data_source[i][1] + " Default"
        elif Identifier.count("Mean") > 0:
            NoordAa = NoordAa + "-x Mean"
            data_source[i][1] = data_source[i][1] + " Mean"
        # if Identifier[0].isdigit():
        #    NoordAa = Identifier[0:6] + Identifier[Identifier.rindex(".lif") + 4:Identifier.rindex(".tif")]
        # else:
        #    NoordAa = Identifier[0:9] + Identifier[Identifier.rindex("-"):]
        Identifier_list.append(NoordAa)
    else:
        Remove_list.append(i)
for rem in range(len(Remove_list) - 1, -1, -1):
    To_delete = Remove_list[rem]
    del data_source[To_delete]


# Returns the unique values from a list. Returns names of the images.
def unique(list1):
    Gross = np.array(list1)
    Boventocht = np.unique(Gross)
    return Boventocht


Merwede = unique(Identifier_list)

# Appends all chloroplasts with the same image name into a nested list

def append_stackimage(Rijn, Lookup):
    IJssel = []
    LookIdent1 = Lookup[:Lookup.index("-x")]
    LookIdent2 = Lookup[Lookup.rindex("-x") + 2:]
    for Lek in range(0, len(Rijn)):
        Walen = Rijn[Lek]
        Rijnen = Walen[1]
        if LookIdent1 in Rijnen and LookIdent2 in Rijnen:
            IJssel.append(Walen)
    return IJssel


# Separates all images with different names into a list and combines the list.
# Calling Bovenmerwede[0] will give all data from first stack.
Bovenmerwede = []
for middeltocht in range(0, len(Merwede)):
    Bovenmerwede.append(append_stackimage(data_source, Merwede[middeltocht]))


def list_variable(Liste, calledTitle):
    Vecht = []
    StichtseVecht = 0
    for OVVecht in range(0, len(Header)):
        if calledTitle in Header[OVVecht]:
            StichtseVecht = OVVecht
            break
    for Qued in range(0, len(Liste)):
        Vechte = Liste[Qued]
        Vecht.append(Vechte[StichtseVecht])
    return Vecht

# Creates a list of the variables Xm, Y, Stack, Label, Radius and Mean of chloroplasts in a single image
XM = []
YM = []
Stacks = []
Number = []
Radius = []
Mean = []
for Que in range(0, len(Bovenmerwede)):
    XM.append(list_variable(Bovenmerwede[Que], "XM"))
    YM.append(list_variable(Bovenmerwede[Que], "YM"))
    Stacks.append(list_variable(Bovenmerwede[Que], "Stack"))
    Number.append(list_variable(Bovenmerwede[Que], "Label"))  # Changes number to name to get name out
    Radius.append(list_variable(Bovenmerwede[Que], "Radius"))
    Mean.append(list_variable(Bovenmerwede[Que], "Mean"))


# Clusters the numbers based on the values of XM, YM and stacks
# Puts the first value in a nested list. Takes the next and checks if it is similar enough
# to put in same list. If not, continue to next list and if nowhere, makes new list.
# Requirements are for XM, YM and stack, Radius, Label and others are clustered based on these requirements.
def clusterchloro(XM1, YM1, stack1, number1, radius1, mean1):
    numbers1 = [[number1[0]]]
    XM11 = [[float(XM1[0])]]
    YM11 = [[float(YM1[0])]]
    stacks11 = [[float(stack1[0])]]
    Radius11 = [[float(radius1[0])]]
    mean11 = [[float(mean1[0])]]
    for Schipbeek in range(1, len(XM1)):
        Bovenregge = 0
        XM_short = float(XM1[Schipbeek])
        YM_short = float(YM1[Schipbeek])
        stack_short = float(stack1[Schipbeek])
        number_short = number1[Schipbeek]
        Radius_short = float(radius1[Schipbeek])
        mean_short = float(mean1[Schipbeek])
        for Eem in range(0, len(XM11)):
            if abs(XM_short - float(XM11[Eem][-1])) <= 4 and abs(
                    YM_short - float(YM11[Eem][-1])) <= 4 and stack_short != stacks11[Eem][-1]:
                XM11[Eem].append(XM_short)
                YM11[Eem].append(YM_short)
                stacks11[Eem].append(stack_short)
                numbers1[Eem].append(number_short)
                Radius11[Eem].append(Radius_short)
                mean11[Eem].append(mean_short)
                Bovenregge = Bovenregge + 1
                break
        if Bovenregge == 0:
            XM11.append([XM_short])
            YM11.append([YM_short])
            stacks11.append([stack_short])
            numbers1.append([number_short])
            Radius11.append([Radius_short])
            mean11.append([mean_short])
    return Radius11, mean11, stacks11, number1


# Takes only chloroplasts that appear more than three stacks. Third percentile is calculated and compared with max value.
# Moreover, thickness is calculated by taking difference between first and last stack and number of images of a chloro in a stack.

Results = []
Double = []
Ratiothirdpercmax = []
for Twentekanaal in range(0, len(XM)):
    ClusteredNumbers = clusterchloro(XM[Twentekanaal], YM[Twentekanaal], Stacks[Twentekanaal], Number[Twentekanaal],
                                     Radius[Twentekanaal], Mean[Twentekanaal])
    for HoofdVaart in range(0, len(ClusteredNumbers[0])):
        if ClusteredNumbers[0][HoofdVaart] in Double:
            # Sometimes naming of images is wrong. This prevents taking two times the same datapoints
            break
        if len(ClusteredNumbers[0][HoofdVaart]) < 4:
            continue
        if "Default" in ClusteredNumbers[3][HoofdVaart]:  # Substitute Default for Mean if measuring unexpanded chloroplasts
            continue
        Nameident = ClusteredNumbers[3][HoofdVaart]
        # Names = Nameident[Nameident.rindex(" - ") + 3:]
        Names = Nameident[Nameident.rindex(" - 2") + 3:]
        # Names = Nameident[Nameident.rindex(".lif") - 7:]
        Maxrad1 = max(ClusteredNumbers[0][HoofdVaart])
        ClusteredNumbers[0][HoofdVaart].sort()
        thirdpercentile = ClusteredNumbers[0][HoofdVaart][int(0.75 * len(ClusteredNumbers[0][HoofdVaart])) - 1]
        ratiopercmax = thirdpercentile / Maxrad1
        Ratiothirdpercmax.append(ratiopercmax)
        AVMean1 = sum(ClusteredNumbers[1][HoofdVaart]) / len(ClusteredNumbers[1][HoofdVaart])
        Thickness1 = int(max(ClusteredNumbers[2][HoofdVaart]) - min(ClusteredNumbers[2][HoofdVaart]))
        count1 = len(ClusteredNumbers[0][HoofdVaart])
        Double.append(ClusteredNumbers[0][HoofdVaart])
        Smallresults = [Names, Maxrad1, thirdpercentile, AVMean1, Thickness1, count1]
        Results.append(Smallresults)

# Calculate the average radius and average mean
Averrad = []
Avermean = []
Averratio = sum(Ratiothirdpercmax) / len(Ratiothirdpercmax)
Stratio = statistics.stdev(Ratiothirdpercmax)
for Oranjekanaal in range(0, len(Results)):
    Averrad.append(Results[Oranjekanaal][1])
    Avermean.append(Results[Oranjekanaal][2])

AverageRadius = sum(Averrad) / len(Averrad)
AverageMean = sum(Avermean) / len(Avermean)

Toprow = ["Name", "Radius", "Third percentile", "Average Mean", "Thickness (# stacks)", "# images per chloro"]

with open(Location + Outputfilename, 'w', newline='') as Diependal:
    # create the csv writer
    writer = csv.writer(Diependal)
    writer.writerow(["Average Radius is: ", AverageRadius])
    writer.writerow(["The average mean is: ", AverageMean])
    writer.writerow(["Ratio third percentile and max is: ", Averratio, "with an stdev of: ", Stratio])
    writer.writerow([""])
    writer.writerow(Toprow)
    for nijevaart in range(0, len(Results)):
        writer.writerow(Results[nijevaart])